#include "gra.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{

    int x=1;

    do
    {
        int rozmiar;
        int ile_w_linii;
        int poziom_cpu;
        cout<<"Kolko i krzyzyk"<<endl<<endl;
        cout<<"Podaj wymiar planszy od 3 do 9"<<endl;
        cin>>rozmiar;
        if(rozmiar<3)
        {
            rozmiar=3;
            cout<<"zostal ustalony minimalny rozmiar 3x3"<<endl;
        };
        if(rozmiar>9)
        {
            rozmiar=9;
            cout<<"zostal ustalony maksymalny rozmiar 9x9"<<endl;
        };
        cout<<"Podaj liczbe znakow potrzebnych do wygrania gry od 3 do 10"<<endl;
        cin>>ile_w_linii;
        if(ile_w_linii<3)
        {
            ile_w_linii=3;
            cout<<"minimum to 3 znaki w linii"<<endl;
        };
        if(ile_w_linii>rozmiar)
        {
            ile_w_linii=rozmiar;
        };
        cout<<"Podaj poziom trudnosci komputera od 1 do 9"<<endl;
        cin>>poziom_cpu;
        if(poziom_cpu<1)
        {
            poziom_cpu=1;
            cout<<"zostal ustalony minimalny poziom"<<endl;
        };
        if(poziom_cpu>9)
        {
            poziom_cpu=9;
            cout<<"zostal ustalony maksymalny poziom"<<endl;
        };

        KiK *kik = new KiK(rozmiar,ile_w_linii,poziom_cpu);
        kik->graj(); // metoda uruchamiajaca gre

        cout<<"Czy chcesz grac dalej?"<<endl<<endl;
        cout<<"Tak ( 1 ) / Nie ( 0 )"<<endl;
        cin>>x;
        system("CLS");
    }
    while(x==1);
    cin.sync();
    cin.get();

    return 0;

}
