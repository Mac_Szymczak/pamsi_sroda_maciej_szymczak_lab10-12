#ifndef gra_h
#define gra_h


class KiK {

private:

int poziom_cpu;
int rozmiar;
int ile_w_linii;
char *plansza;
int *zaznaczone;

public:


KiK(int rozmiar, int ile_w_linii, int poziom_cpu); // Konstruktor gry - ustawia plansze i zaznaczone pola na domyœlne

~KiK();


void rysujPlansze();

bool czyRemis(bool flaga);

bool czyWygrana(char gracz, bool flaga); // Metoda sprawdzaj¹ca, czy gracz lub komputer wygra³

int miniMax(char gracz, int poziom); //Metoda wykonuj¹ca algorytm minimax

int ruchKomputera(); // Metoda wykonuj¹ca ruch komputera

void ruch(char &gracz); //Metoda wykonuj¹ca ruch gracza, czyli nas grajacych


void graj(); // Metoda uruchamiaj¹ca grê !!!

};

#endif
