#include "gra.h"
#include <iostream>
#include <stdlib.h>


using namespace std;

KiK::KiK(int rozmiar,int ile_w_linii, int poziom_cpu)
{
    this->rozmiar = rozmiar;
    this->ile_w_linii = ile_w_linii;
    this->poziom_cpu = poziom_cpu;
    plansza = new char [rozmiar*rozmiar+1];
    zaznaczone = new int[rozmiar*rozmiar+1];

    for(int i = 0; i < rozmiar*rozmiar+1; i++)
    {
        plansza[i] = ' '; // plansza wypełniona przez puste pola
        zaznaczone[i]=0; // wartosci pol sa ustawione jako '0' czyli nieajete
    }

}

KiK::~KiK()
{

    delete zaznaczone;
    delete plansza;

}

void KiK::rysujPlansze() // funkcja do narysowania planszy do gry z dostepnych znakow standardowych
{

    for(int i = 1; i <= rozmiar*rozmiar; i++)
    {
        if( plansza[i] != ' ' ) { cout<<" "<<plansza[i]<<" ";} else if(i<=9) { cout<<" "<<i<<" "; } else{cout<<" "<<i;}
        if(i%rozmiar)
            cout<<"|";
        else if(i!=rozmiar*rozmiar)
        {
            cout<<"\n---";
            for(int j=1;j<rozmiar;j++) cout<<"+---";
            cout<<"\n";
        }

        else
            cout<<endl;

    }

}

bool KiK::czyRemis(bool flaga) // w metodzie KiK::graj zmienna typu bool jest domyslnie ustawiona na false
{

    for(int i = 1; i < rozmiar*rozmiar+1; i++)
    {

        if(plansza[i]==' ') // sprawdzamy czy jest jakiekolwiek puste pole, REMIS mo¿e wyst¹piæ tylko!!! wtedy kiedy ca³a plansza jest wype³niona
            return false;   // jeœli jest, to zwracamy fa³sz - nie mo¿e byæ remisu !!!

    }

    if(!flaga) //dopoki nie bedzie remisu
    {
        // jeœli jest remis to wypisujemy komunikat i koñczymy; jedynie w minimax przyjmuje true, aby to !!!  siê nie wykona³o!!!
        system("CLS");
        cout<<"Kolko i krzyzyk"<<endl<<endl;
        rysujPlansze();
        cout<<endl;
        cout<<"REMIS!"<<endl<<endl;

    }
    return true; // zwracamy true je¿eli jest remis

}

////////////////////////
bool KiK::czyWygrana(char gracz, bool flaga)
{

    bool czyWygrana = false;
    int ileZRzedu=0;
    // sprawdzenie czy jest wymagana ilosc znakow w wierszu
    for(int i = 0; i<rozmiar; i++)
    {
        for(int j = (1+i*rozmiar); j<=(rozmiar+i*rozmiar); j++) //
        {
            if(plansza[j]==gracz)
            {
                ileZRzedu++;
                if(ileZRzedu==ile_w_linii) czyWygrana = true;
            }
            else ileZRzedu = 0;
        }
        ileZRzedu = 0;
    }

    //sprawdzenie czy jest wymagana ilosc znakow w kolumnie
    for(int i = 0; i<rozmiar; i++)
    {
        for(int j = (1+i); j <= ((1+i)+(rozmiar-1)*rozmiar); j += rozmiar)
        {
            if(plansza[j]==gracz)
            {
                ileZRzedu++;
                if(ileZRzedu==ile_w_linii) czyWygrana = true;

            }
            else ileZRzedu = 0;
        }
        ileZRzedu = 0;
    }

    //sprawdzenie czy jest wymagana ilosc znakow po przekatnej w lewo
    for(int i = ile_w_linii-1; i<rozmiar; i++)
    {
        for(int j = i+1; j <= (i*rozmiar)+1; j += rozmiar-1)
        {
            if(plansza[j]==gracz)
            {
                ileZRzedu++;
                if(ileZRzedu==ile_w_linii) czyWygrana = true;

            }
            else ileZRzedu = 0;
        }
        ileZRzedu = 0;
    }

    //sprawdzenie czy jest wymagana ilosc znakow po skosie w lewo jezeli ilosc znakow wymaganych
    // do wygranej jest mniejsza niz rozmiar
    if(ile_w_linii<rozmiar)
    {
        for(int i = rozmiar+rozmiar; i<=(rozmiar*rozmiar)-(ile_w_linii-1)*rozmiar; i += rozmiar)
        {
            for(int j = i; j <=i +((((rozmiar*rozmiar)-i)/rozmiar)*(rozmiar-1)) ; j += rozmiar-1)
            {
                if(plansza[j]==gracz)
                {
                    ileZRzedu++;
                    if(ileZRzedu==ile_w_linii) czyWygrana = true;

                }
                else ileZRzedu = 0;
            }
            ileZRzedu = 0;
        }
    }

    //sprawdzenie czy jest wymagana ilosc znakow po przekatnej w prawo
    for(int i = 1; i<=(rozmiar-ile_w_linii)+1; i++)
    {
        for(int j = i; j <= (rozmiar-i+1)*rozmiar; j += rozmiar+1)
        {
            if(plansza[j]==gracz)
            {
                ileZRzedu++;
                if(ileZRzedu==ile_w_linii) czyWygrana = true;

            }
            else ileZRzedu = 0;
        }
        ileZRzedu = 0;
    }

    //

    //sprawdzenie czy jest wymagana ilosc znakow po skosie w prawo jezeli ilosc znakow wymaganych
    // do wygranej jest mniejsza niz rozmiar
    if(ile_w_linii<rozmiar)
    {
        for(int i = 1+rozmiar; i<=1+(ile_w_linii-1)*rozmiar; i += 4)
        {
            for(int j = i; j <= i + (rozmiar-(i/rozmiar)-1)*(rozmiar+1); j += rozmiar+1)
            {
                if(plansza[j]==gracz)
                {
                    ileZRzedu++;
                    if(ileZRzedu==ile_w_linii) czyWygrana = true;

                }
                else ileZRzedu = 0;
            }
            ileZRzedu = 0;
        }
    }


    //

    if(czyWygrana)   //jeżeli nastąpiłą wygrana
    {

        if(!flaga)  // flaga zostala ustawiona na false i zmienia sie to tylko przy wygranej
        {
            system("CLS");
            cout<<"Kolko i krzyzyk"<<endl<<endl;
            rysujPlansze();
            if(gracz=='o')
            {
            cout<<endl<<"Wygrana!"<<endl<<endl;
            }
            else
            {
            cout<<endl<<"Porazka..."<<endl<<endl;
            }
        }

        return true;
    }

    return false; // jeœli nie, zwracamy fa³sz

}

///////////////////////////////////////////////////////////////MINMAX/////////////////////////////////////////////////////////////////////////////////////
int KiK::miniMax(char gracz,int poziom)
{

    int m, mmx; // zmienne pomocnicze, m - przypisanie wyliczonej wartoœci z rekurencyjnie wywo³anego minimax, mmx - zmienna na punkty

    if(czyWygrana(gracz, true))   // sprawdzamy czy ktos nie wygral
    {

        if(gracz=='x')
            return 1;   // jeœli gracz, to zwracamy 1
        else
            return -1;  // jeœli komputer, to zwracamy -1

    }//minmax wykonuje sie dopoki az ktos wygra,we wszystkie wolne pola!!! wstaia sobie, symuluje ca³¹ gre do konca, za ka¿dym ruchem(na dany moment)


    if(czyRemis(true))   // sprawdzamy, czy nie nast¹pi³ remis
    {

        return 0;

    }

//ZAMIANA GRACZA//////////////////////
    if(gracz=='x')
        gracz = 'o';
    else
        gracz = 'x';
////////////////////////////////////////////

    if(gracz=='o')
        mmx = 10;  // jeœli sprawdzamy mo¿liwoœci dla gracza, ustalamy maksymaln¹ granicê pkt na 10 !!!
    else
        mmx = -10; // jeœli sprawdzamy mo¿liwoœci dla komputera, ustalamy maksymaln¹ granicê pkt na -10 !!!
///////////////////////////////////

    for(int i = 1; i <= rozmiar*rozmiar; i++) // ca³¹ plansze przelatujemy
    {

        if(plansza[i] == ' ')   // sprawdzamy rekurencyjnie minimax dla wszystkich wolnych pól - badamy wszystkie mo¿liwe ruchy i punkty do zdobycia
        {

            plansza[i] = gracz;

            if(poziom != 0) m = miniMax(gracz,poziom--); // minmax wykonuje sie rekurencyjnie w petli tyle razy jaki poziom ustawimy


            plansza[i] = ' ';

            if(((gracz == 'o') && (m < mmx)) || ((gracz == 'x') && (m > mmx)))// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                mmx = m;
        }

    }

    return mmx; // zwracamy punkty !!!

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int KiK::ruchKomputera()
{

    int ruch, m, mmx;

    mmx = -10;

    for(int i = 1; i <= rozmiar*rozmiar; i++)
    {       //////////////////////////////////////////

        if(plansza[i]==' ')   // sprawdzamy rekurencyjnie minimax dla wszystkich wolnych pól - badamy wszystkie mo¿liwe ruchy i punkty do zdobycia
        {

            plansza[i] = 'x';
            //////////MINMAX/////////
            m = miniMax('x',poziom_cpu);
            /////////////////////
            plansza[i] = ' ';

            if(m > mmx) // !!!!!!!!!!!!!!!!!!
            {

                mmx = m;
                ruch = i; // ustawiamy ruch który komputer wykona na podstawie wyliczonych punktów

            }

        }
            ///////////////////////////////////////
    }

    return ruch; // zwracamy numer ruchu

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KiK::ruch(char &gracz)
{

    int wybor;

    system("CLS");
    cout<<"Kolko i krzyzyk"<<endl<<endl<<"Wybierz numer pola"<<endl<<endl;
    rysujPlansze();

    if(gracz=='o')   // jeœli graczem jest cz³owiek
    {
        cout<<endl;
        cout<<"Czlowiek - ruch: ";
        do
        {
            cin>>wybor; // czekamy a¿ wpisze numer pola
            if(zaznaczone[wybor]==1) cout<<"Wybierz ponownie, pole "<<wybor<<" jest zajête!"<<endl;
        }
        while(zaznaczone[wybor]==1);
        zaznaczone[wybor]=1; // oznaczamy pole jako zajête, jeœli jeszcze nie bylo oznaczone

    }
    else   // jeœli graczem jest komputer !!!
    {

        wybor=ruchKomputera(); // dostajemy od komputera info o ruchu jaki wykona³
        // dostajemy indeks
        cout<<endl;
        cout<<"Komputer - ruch: "<<wybor<<endl;
        zaznaczone[wybor]=1; // oznaczamy pole jako zajête

    }

    if((wybor >= 1) && (wybor <= rozmiar*rozmiar) && (plansza[wybor]==' '))//////////////////////
    {

        plansza[wybor] = gracz; // oznaczamy na planszy znaczek czyli 0 lub X gracza( komputer lub czlowiek)

    }


    if(gracz=='o') // zmiana gracza na przeciwnego
        gracz = 'x';
    else
        gracz = 'o';
}

void KiK::graj()
{

    char gracz = 'o'; // czlowiek zaczyna


    while(!czyWygrana('x', false) && !czyWygrana('o', false) && !czyRemis(false)) // gramy dopóki któraœ z tych funkcji nie zwróci prawdy
    {
        ruch(gracz);
    }


}
